from behave.__main__ import main as behave_main
from allure_behave.hooks import allure_report

behave_main("features")
allure_report("/home")
