from behave import *
from object_data.payloads import *
from object_data.headers import *
from object_data.responses import *
import requests
import logging as log


@given(u'the server is up')
def given_the_server_is_up(context):
    payload = build_payload("50")
    url = get_url('POSITIVE')
    response = requests.post(url, data=payload, headers=headers)
    assert ports['POSITIVE'] in response.text
    assert port_sample_src in response.text
    assert port_sample_dest in response.text
    assert "1" in response.text
    assert 'test'+speed['50'] in response.text

@when(u'user tries to load a payload with speed {speed_key}, term {term} and discussion {discussion} with src port {src}')
def when_user_tries_to_load_a_payload(context, speed_key, term, discussion, src):
    url = get_url(src)
    payload = build_full_payload(speed_key, term, discussion)
    context.data = [port_sample_src, port_sample_dest, speed[speed_key], term, discussion]
    response = requests.post(url, data=payload, headers=headers)
    context.response = response

@then(u'the server must return {resp}')
def then_the_server_must_return(context, resp):
    log.info("Retrieved =>" + context.response.text)
    expected = get_response(resp, context.data)
    log.info("Expected => " + expected)
    assert expected in context.response.text
    