speed = {
    "50" : "50Mbps",
    "50_UPPER" : "50MBPS",
    "50_LOWER" : "50mbps",
    "100" : "100Mbps",
    "500" : "500Mbps",
    "1000" : "1000Mbps",
    "1G" : "1Gbps",
    "50G" : "50Gbps",
    "INVALID_1T": "1Tbps",
    "INVALID_LOW": "49Mbps",
    "100000K" : "100000Kbps"
}

def build_full_payload(key, term, description):
    return '''
    {{
        "speed":"{}",
        "term":{},
        "description":"{}"
    }}
    '''.format(speed[key], term, description)

def build_payload(key):
    speed_value = speed[key]
    return '''
    {{
        "speed":"{0}",
        "description":"test{0}"
    }}
    '''.format(speed_value)

ports = {
    "NEGATIVE": "-1",
    "ZERO": "0",
    "POSITIVE": "8089"
}

'''
These ports simulates the reserverd ports by the
operational system. You can change the value in range
for most siginficant values.
'''
reserved_ports = (x for x in range(1,1023))
