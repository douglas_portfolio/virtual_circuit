from .payloads import ports
from random import randint

PORT_KEY = 'POSITIVE'

def get_url(port_key):
    if port_key is not None and port_key is not '':
        PORT_KEY = port_key
    return 'http://localhost:8000/virtual-circuit/{0}/{1}'.format(str(port_sample_src), str(port_sample_dest))

def get_sample_src_port():
    port = PORT_KEY if isinstance(PORT_KEY, int) else int(ports[PORT_KEY])
    return port

def get_sample_dest_port():
    port = PORT_KEY if isinstance(PORT_KEY, int) else int(ports[PORT_KEY])
    return port + randint(1,1000)

headers = {
    'Content-Type': "text/plain",
    'Accept': "*/*"
    }

port_sample_src = str(get_sample_src_port()).strip()
port_sample_dest = str(get_sample_dest_port()).strip()
