import logging as log

def get_invalid_speed_err_message(speed):
    return 'Invalid Speed provided: {}'.format(speed)

TERM_ERROR_MESSAGE = 'term must be greater than 0'
PORT_ERROR_MESSAGE = 'src_port must be a positive integer'

def get_response(key, value):
    resp = ''
    if key =='PAYLOAD':
        resp = '{{"src_port":{0},"dest_port":{1},"speed":"{2}","term":{3},"description":"{4}"}}'.format(value[0], value[1], value[2], value[3], value[4])
    elif 'INVALID' in key:
        resp = 'Error: ' + get_invalid_speed_err_message(value[2])
    elif 'NEG_PORT' in key:
        resp = PORT_ERROR_MESSAGE
    elif 'ZERO_PORT' in key:
        resp = PORT_ERROR_MESSAGE
    elif 'NEG_TERM' in key:
        resp = TERM_ERROR_MESSAGE
    else:
        resp = value
    return resp