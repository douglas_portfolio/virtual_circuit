Feature: The virtual cirtuit server
    As an user of the service
    I want to set source and destination ports and the speed of them
    So that a link must be created between them

Scenario Outline: Server must be up
    Given the server is up
    When user tries to load a payload with speed <speed_key>, term <term> and discussion <discussion> with src port <src>
    Then the server must return <resp>
    Examples:
    | speed_key   | term | discussion      | src      | resp        |
    | 50          |  1   | ON LIST         | POSITIVE | PAYLOAD     |
    | 50_UPPER    | 10   | UPPER CASE      | POSITIVE | PAYLOAD     |
    | 100         | 10   | ON LIST         | POSITIVE | PAYLOAD     |
    | 500         | 10   | ON LIST         | POSITIVE | PAYLOAD     |
    | 1000        | 10   | ON LIST         | POSITIVE | PAYLOAD     |
    | 1G          | 10   | ON LIST         | POSITIVE | PAYLOAD     |
    | 50G         | 10   | ON LIST         | POSITIVE | PAYLOAD     |
    | INVALID_1T  | 10   | INVALID SPEED   | POSITIVE | INVALID_1T  |
    | INVALID_LOW | 10   | INVALID SPEED   | POSITIVE | INVALID_LOW |
    | 100000K     | 10   | OTHER UNIT      | POSITIVE | PAYLOAD     |
    | 50          | 10   | NEG PORT        | NEGATIVE | NEG_PORT    |
    | 50          | 10   | ZERO PORT       | ZERO     | ZERO_PORT   |
    | 50          |-1    | NEG TERM        | POSITIVE | NEG_TERM    |