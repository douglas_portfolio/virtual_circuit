# virtual_circuit

In this repository I've made a behave test automation from a virtual circuit creator simple rest service.

## How to prepare your environment
- mkvirtualenv packetFabric
- pip3.6 install -r requirements.txt

## How to run
There are two ways to run this code:
- using main (logs will be disabled):
```shellscript
python3.6 main.py
```
- using behave (with arg to allow logs):
```shellscript
behave --no-capture
```

## CI & CD
To see the code running in continuous integration please [CLICK HERE]().


